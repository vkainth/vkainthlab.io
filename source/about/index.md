---
title: About
date: 2018-06-26 17:30:28
description: The about page
---

# Hello

Hello and welcome to my professional blog. My name is Vibhor Kainth and I am a
software engineer living in Texas, USA. Here, I will detail my adventures with
software development.

## Writing

This section will contain everything I will ever write on the topic of software
engineering. I'll make stuff, break stuff, and rebuild most stuff and this is
the spot where I will detail that journey. Since software development is simply
not done without code, I will detail small projects (upto two weeks) here.

### Technologies

For the most part I like creating websites as its easier to view the final
product without much effort on the user's side. But, I will also be working
on [Unity Game Enginer](https://unity3d.com/), creating awesome games.

## Projects

This is the section that will contain most of my projects. These are the projects
that take far longer and require consistent effort and committment. I will detail
the end results and the steps to achieve that result in the Writing section.
I have shifted to
[Gitlab](https://www.gitlab.com) for all of my personal projects and you will
find most of my contributions there.
