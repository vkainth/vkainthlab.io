---
title: Hello Hexo
date: 2018-06-26 17:39:35
tags:
- Static Site Generator
- Hexo
categories:
- Software Development
---

# Hi

Welcome to the first post.

## Introduction

This website was quickly scaffolded using [Hexo](https://www.hexo.io), the static
site generator that uses JavaScript. The theme is called
[Cactus](https://github.com/probberechts/hexo-theme-cactus)
and all rights for the theme are reserved by the author of that theme.

Let's go over some steps required to setup your own hexo website.

## Quickstart

- Run `npm install -g hexo-cli` from your terminal

  - Alternatively, use `yarn global add hexo-cli`

- In your directory, run `hexo init blog` to create a new hexo directory

- Run `cd blog` and execute `yarn install` or `npm install`

- Run `hexo generate` followed by `hexo server` to view your website using the
  default theme

Congratulations! You now have a simple website running on localhost.

## Changing themes

Let's install the Cactus theme.

- In `blog/`, run `git clone https://github.com/probberechts/hexo-theme-cactus themes/cactus`

- Run `cd themes/cactus` and execute `rm -rf .git/` to remove this theme from git

  - **Note** This is not the correct way to add other git tracked files to your repository.
    I will use git submodules in the future but I wanted to get this set up quickly.

- In your `_config.yml`, in the root directory (`blog/`), change `theme: landscape`
  to `theme: cactus`

And that's it! Now, you have the Cactus theme installed and ready. You can run
`hexo generate` followed by `hexo server` to view the final result.

## Customizing the theme

In your `_config.yml`, add the following:

```yml
theme_config:
  colorscheme: dark
  projects_url: LINK_TO_YOUR_PROJECTS
  nav:
    Home: /
    About: /about/
    Writing: /archives/
    Projects: LINK_TO_YOUR_PROJECTS

  social_links:
    gitlab: LINK_TO_YOUR_GITLAB
    github: LINK_TO_YOUR_GITHUB
    twitter: LINK_TO_YOUR_TWITTER

  posts_overview:
    show_all_posts: false
    post_count: 10

  rss: atom.xml
```

Save and run `hexo generate` and `hexo server` to view your results.

## Further details

- [Hexo docs](https://hexo.io/docs/)

- [Cactus Theme Readme](https://github.com/probberechts/hexo-theme-cactus)
