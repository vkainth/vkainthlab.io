---
title: Three simple computer science problems
date: 2018-06-28 17:58:56
tags:
- HTML
- CSS
- Javascript
- Bulma
categories:
- Software Development
---

# Hi

Welcome to my blog. Today, I created a website for three simple Computer Science
problems that I solved using JavaScript. Link below. That link contains the
description of each problem and my solution for them.

## Link

[https://vkainth.gitlab.io/js-exercises/](https://vkainth.gitlab.io/js-exercises/)

## Reason

I feel as if my reasoning skills are getting rusty. So, I though I would work
on three simple programming challenges. Also, this exercise helped me work
my JavaScript muscles so double-win.

## Technology stack

- HTML and CSS

- Javascript

- [Bulma](https://www.bulma.io)

## Improvements

- Input validation

- Edge cases testing
