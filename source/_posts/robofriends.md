---
title: Robofriends, a simple React app
date: 2018-07-02 14:49:00
tags:
- HTML
- CSS
- JSX
- Javascript
- React
- Tachyons
categories:
- Web development
---

# Hi

Welcome to my blog. Today, I created a React.js application. Link below.

## Link

[https://vkainth.gitlab.io/robofriends/](https://vkainth.gitlab.io/robofriends/)

## Introduction

This simple application introduces all the basics of React development, including
Components, Props, State, and Lifecycle Methods. Further, the application uses
2 APIs: [JSON Placeholder API](https://jsonplaceholder.typicode.com/) for
fetching users and the [Robohash API](http://robohash.org/) for fetching the
robot images.

## Purpose

My sole purpose was to learn React development and quickly get started on the
path of becoming a competent web developer. This simple application taught me
the basics of the React ecosystem. I have a few projects planned for the future.

## Motivation

The [Complete Web Developer in 2018](https://www.udemy.com/the-complete-web-developer-in-2018/)
course by [Andrei Neagoie](https://github.com/aneagoie) details all the steps
necessary for this application.

## Future Improvements

- Add Redux
