---
title: Todo app in Native Javascript
date: 2018-06-28 17:46:00
tags:
- HTML
- CSS
- Bootstrap
- Native Javascript
- Demo
categories:
- Software Development
---

# Hi

Welcome to my blog. A few days ago, I built a ToDo App in Javascript, the
link is below.

## Link

[https://vkainth.gitlab.io/super-basic-todo/](https://vkainth.gitlab.io/super-basic-todo/)

## Reason

I like learning things by building. I feel an immense amount of satisfaction
when I see something I made from any device. Since I'm relearning some
aspects of Javascript, I thought might as well make a ToDo app. After all,
the classics are the best. Always.

## Technology stack

- HTML and CSS

- Native Javascript

- Bootstrap

### Choosing Bootstrap

Simply put, Bootstrap is **the** de-facto standard for making responsive websites
that look professional and perform well. Might as well use a framework instead
of creating my own.
